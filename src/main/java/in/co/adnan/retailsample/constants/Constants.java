package in.co.adnan.retailsample.constants;

public interface Constants {

	double EMPLOYEE_DISCOUNT_PERCENTAGE = 30.00;
	double AFFILIATE_DISCOUNT_PERCENTAGE = 10.00;
	double CUTOMER_DISCOUNT_PERCENTAGE = 5.00;
	int CUTOMER_DISCOUNT_ELLIGIBILITY = 24; // months
	int FIXED_AMOUNT_LIMIT = 100; // months
	int FIXED_AMOUNT_LIMIT_DISCOUNT = 5; // months
}
