package in.co.adnan.retailsample.request;

import java.util.List;

import in.co.adnan.retailsample.model.Product;

public class AddOrderProductsRequest {

	private int userId;
	private int orderId;
	private List<Product> products;
	
	public AddOrderProductsRequest() {
		super();
	}
	
	

	public AddOrderProductsRequest(int userId, int orderId, List<Product> products) {
		super();
		this.userId = userId;
		this.orderId = orderId;
		this.products = products;
	}



	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	

}
