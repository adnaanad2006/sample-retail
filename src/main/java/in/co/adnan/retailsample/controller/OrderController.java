package in.co.adnan.retailsample.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.co.adnan.retailsample.constants.Constants;
import in.co.adnan.retailsample.model.Product;
import in.co.adnan.retailsample.model.User;
import in.co.adnan.retailsample.request.AddOrderProductsRequest;
import in.co.adnan.retailsample.response.AddOrderProductResponse;
import in.co.adnan.retailsample.response.BaseResponse;
import in.co.adnan.retailsample.services.UserService;

@RestController
@RequestMapping("/orders")
public class OrderController {

	@Autowired
	UserService userService;

	
	@PostMapping("addToCart")
	public BaseResponse<AddOrderProductResponse> addProducts(@Valid @RequestBody AddOrderProductsRequest req) {

		
		
		int userId = req.getUserId();
		User user = userService.getUserById(userId);
		List<Product> products = req.getProducts();


		double subTotal = 0.00;
		double discount = 0.00;
		double grandTotal = 0.00;
		
		

		if(products != null && !products.isEmpty()) {
			subTotal = products.stream().mapToDouble(p ->  p.getPrice()*p.getQuantity() ).sum();
			
			discount = getDiscountAmount(user, products);
			
			grandTotal = subTotal - discount;
		}
		
		

		return new BaseResponse<>(1, "Cart Updated successfully", new AddOrderProductResponse(userId, subTotal, discount, grandTotal));
	    
	}
	
	/*
	 * Calculate User Discount for cart product
	 * 
	 * */
	public double getDiscountAmount(User user, List<Product> products) {
		


		double subTotal;
		double discount;
		double percentDiscount = 0.00;
		double fixedDiscount;

		// calculate cart total
		subTotal = products.stream().mapToDouble(p ->  p.getPrice()*p.getQuantity() ).sum();

		List<Product> groceryProducts = products.stream().filter(p ->  p.getCategory().getName().equals("Grocery") ).collect(Collectors.toList());
		
		if(user != null && groceryProducts.isEmpty()) { // 
			String role = user.getRole().getRoleName();

	
			
			switch(role) {
				case "employee": 
					percentDiscount = subTotal * Constants.EMPLOYEE_DISCOUNT_PERCENTAGE / 100;
					break;
				case "affiliate": 
	
					percentDiscount = subTotal * Constants.AFFILIATE_DISCOUNT_PERCENTAGE / 100;
					
					break;
				case "customer": 
					
					Date createdDate = user.getDateCreated();
					Calendar pastDate = Calendar.getInstance();
					pastDate.add(Calendar.MONTH, (Constants.CUTOMER_DISCOUNT_ELLIGIBILITY * -1));
					
					// If user created date is less than past date
					if(createdDate.compareTo(pastDate.getTime()) < 0) {
						percentDiscount = subTotal * Constants.CUTOMER_DISCOUNT_PERCENTAGE / 100;
					}
					
					break;
				default:
					
			}

		}

		fixedDiscount = getFixedDiscount(subTotal);
		
		
		discount = getFinalDiscount(subTotal, percentDiscount, fixedDiscount);

		
		return discount;
	}
	
	/*
	 * Calculate fixed discount on cart total
	 * 
	 * */
	public double getFixedDiscount(double subTotal) {
		
		
		return ( Math.floor(subTotal / Constants.FIXED_AMOUNT_LIMIT) ) * Constants.FIXED_AMOUNT_LIMIT_DISCOUNT;
	}
	
	/*
	 * Calculate final discount 
	 * 
	 * */
	public double getFinalDiscount(double subTotal, double percentDiscount, double fixedDiscount) {
		

		double finalDiscount = percentDiscount + fixedDiscount;
		
		// Final discount should not go in negative
		if(finalDiscount > subTotal ) {
			finalDiscount = subTotal;
		}
		
		return finalDiscount;
	}
	
	
}
