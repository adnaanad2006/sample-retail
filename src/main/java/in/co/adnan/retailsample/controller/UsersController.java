package in.co.adnan.retailsample.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import in.co.adnan.retailsample.model.User;
import in.co.adnan.retailsample.services.UserService;

@RestController
@RequestMapping("/users")
public class UsersController {

	@Autowired
	UserService userservice;
	
	@GetMapping("")
	public List<User> getAllUsers() {
		return userservice.getAllUsers();
	}
	

	
	@PostMapping("")
	public ResponseEntity<?> addRandomUser(@RequestBody User user) {
		

		userservice.addRandomUser(user);
		 
	    String uri = ServletUriComponentsBuilder
	      .fromCurrentServletMapping()
	      .path("/users/{id}")
	      .buildAndExpand(user.getId())
	      .toString();
	    HttpHeaders headers = new HttpHeaders();
	    headers.add("Location", uri);
	 
	    return new ResponseEntity<>(user, headers, HttpStatus.CREATED);
	    
	}
	
}
